package com.example.eljoker.salama.adapters;

import android.view.View;

interface ItemClickListener
{
    void onClick(View view, int position);
}
