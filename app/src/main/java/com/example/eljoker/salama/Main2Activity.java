package com.example.eljoker.salama;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity
{

    ArrayList <String> contacts;
    ArrayList <Integer> num;
    ArrayList <Integer> img;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        lv = findViewById(R.id.lv);
        contacts = new ArrayList<>();
        contacts.add("ahmed");
        contacts.add("3sam");

        img = new ArrayList<>();
        img.add(R.drawable.about);
        Lv_Adapter adapter = new Lv_Adapter(this, R.layout.activity_main2, contacts);
        lv.setAdapter(adapter);

    }

}
