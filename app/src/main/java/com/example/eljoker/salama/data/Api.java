package com.example.eljoker.salama.data;


import com.example.eljoker.salama.models.UserInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("UserInfo")
    Call<UserInfo> searchByNumber(@Query("Number") String number);
    @GET("UserInfo")
    Call<UserInfo> searchByPhone(@Query("Phone") String phone);
}
