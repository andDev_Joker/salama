package com.example.eljoker.salama.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eljoker.salama.R;
import com.example.eljoker.salama.models.product_type_model;
import com.example.eljoker.salama.product_types;
import com.example.eljoker.salama.type_details;

import java.util.ArrayList;

public class product_type_apdapter extends RecyclerView.Adapter<product_type_apdapter.Holder>
{
    Context context;
    ArrayList<product_type_model> arrayList;

    public product_type_apdapter(Context context, ArrayList<product_type_model> arrayList)
    {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.product_type_row, parent, false);
        Holder holder = new Holder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position)
    {
        final product_type_model type_model = arrayList.get(position);
        Glide.with(context).load(type_model.getType_img()).into(holder.img_type);
        holder.type_name.setText(type_model.getType_name());
        holder.type_price.setText(String.valueOf(type_model.getType_price()));
//                + " " + "L.E");
        holder.type_km.setText(String.valueOf(type_model.getType_km()));
//                + " " + "K.M");
//        holder.type_desc.setText(String.valueOf(type_model.getType_desc()));
/*
        holder.setItemClickListener(new ItemClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                String id = type_model.getType_id();
                Intent intent = new Intent(context, type_details.class);
                intent.putExtra("typeId", id);
                context.startActivity(intent);

            }
        });
*/

    }

    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder
    {
        ImageView img_type;
        TextView type_name, type_price, type_km, type_desc;
        public Holder(View itemView)
        {
            super(itemView);
            img_type = itemView.findViewById(R.id.img_type);
            type_name = itemView.findViewById(R.id.type_name);
            type_price = itemView.findViewById(R.id.type_price);
            type_km = itemView.findViewById(R.id.type_km);
//            type_desc = itemView.findViewById(R.id.type_desc);
        }
    }
}
