package com.example.eljoker.salama.models;

public class product_type_model
{
    private String toolbar_name;
    private String type_name;
    private String type_img;
    private String type_id;
    private int type_price;
    private int type_km;
    private String desc;

    public String getDesc()
    {
        return desc;
    }
    public String getToolbar_name() {
        return toolbar_name;
    }

    public void setToolbar_name(String toolbar_name) {
        this.toolbar_name = toolbar_name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getType_img() {
        return type_img;
    }

    public void setType_img(String type_img) {
        this.type_img = type_img;
    }


    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public int getType_price() {
        return type_price;
    }

    public void setType_price(int type_price) {
        this.type_price = type_price;
    }

    public int getType_km() {
        return type_km;
    }

    public void setType_km(int type_km) {
        this.type_km = type_km;
    }
}
