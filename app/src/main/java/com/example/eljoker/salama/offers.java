package com.example.eljoker.salama;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.eljoker.salama.adapters.offers_Adapter;
import com.example.eljoker.salama.models.offer_model;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class offers extends AppCompatActivity
{
    String url;
    RecyclerView rv_offer;
    ArrayList<offer_model> arrayList;
    RequestQueue mRequestQueue;
    offers_Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        rv_offer = findViewById(R.id.rv_offer);
        arrayList = new ArrayList<>();
        adapter = new offers_Adapter(arrayList, getApplicationContext());
        rv_offer.setAdapter(adapter);
        rv_offer.setLayoutManager(new LinearLayoutManager(this));
        mRequestQueue = Volley.newRequestQueue(this);
        parsing();
    }

    private void parsing()
    {
        final ProgressDialog dialog =new ProgressDialog(this);
        dialog.setMessage("تحميل ....");
        dialog.show();
        url = "http://salamakamel-001-site1.etempurl.com/api/manager/GetOffers";
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response)
            {
                for (int i = 0; i < response.length(); i++)
                {
                    try {
                        JSONObject object = response.getJSONObject(i);
                        String name = object.getString("Name");
                        String photo = object.getString("Photo");
                        offer_model model = new offer_model();
                        model.setName(name);
                        model.setPhoto(photo);
                        arrayList.add(model);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
                dialog.dismiss();
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }
}
