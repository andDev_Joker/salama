package com.example.eljoker.salama.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.eljoker.salama.R;
import com.example.eljoker.salama.models.About_Model;

import java.util.ArrayList;

public class About_Adapter extends RecyclerView.Adapter<About_Adapter.MyHolder>
{
    Context context;
    ArrayList<About_Model> arrayList;

    public About_Adapter(Context context, ArrayList<About_Model> arrayList)
    {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.about_row, parent, false);
        MyHolder holder = new MyHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position)
    {
        final About_Model about_model = arrayList.get(position);
        Glide.with(context).load(about_model.getPhoto()).into(holder.img_about);

    }

    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder
    {
        ImageView img_about;
        public MyHolder(View itemView)
        {
            super(itemView);
            img_about = itemView.findViewById(R.id.img_about);
        }
    }
}
