package com.example.eljoker.salama;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.eljoker.salama.data.ApiHelper;
import com.example.eljoker.salama.models.Orders;
import com.example.eljoker.salama.models.UserInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultsActivity extends AppCompatActivity {

    TextView noResult;
    List<Orders> orders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        noResult=findViewById(R.id.noResult);
        if (getIntent().getExtras() != null) {
            String query = getIntent().getStringExtra("QUERY");
            ApiHelper.getApiService().searchByNumber(query).enqueue(new Callback<UserInfo>() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                    if (response.isSuccessful()) {
                        orders = response.body().getOrders();
                        if (orders.size() == 0) {
                            noResult.setVisibility(View.VISIBLE);
                        } else {
                            noResult.setVisibility(View.GONE);
                            TableLayout tableLayout = findViewById(R.id.resultsTable);
                            TableRow tr_head = new TableRow(getApplicationContext());
                            tr_head.setBackgroundColor(getResources().getColor(R.color.colorTableSecond));
                            tr_head.setLayoutParams(new TableRow.LayoutParams(
                                    TableRow.LayoutParams.FILL_PARENT,
                                    TableRow.LayoutParams.WRAP_CONTENT));

                            TextView label_product = new TextView(getApplicationContext());
                            label_product.setText("المنتجات");
                            label_product.setTextSize(25);
                            label_product.setTextColor(R.color.colorBLack);
                            label_product.setTextColor(Color.BLACK);
                            label_product.setGravity(Gravity.CENTER);
                            label_product.setPadding(10, 10, 10, 10);
                            tr_head.addView(label_product);

                            TextView label_CNumber = new TextView(getApplicationContext());
                            label_CNumber.setText("رقم العداد");
                            label_CNumber.setTextSize(25);
                            label_CNumber.setTextColor(R.color.colorBLack);
                            label_CNumber.setGravity(Gravity.CENTER);
                            label_CNumber.setPadding(10, 10, 10, 10);
                            tr_head.addView(label_CNumber);

                            TextView label_date = new TextView(getApplicationContext());
                            label_date.setText("التاريخ");
                            label_date.setTextSize(25);
                            label_date.setTextColor(R.color.colorBLack);
                            label_date.setGravity(Gravity.CENTER);
                            label_date.setPadding(10, 10, 10, 10);
                            tr_head.addView(label_date);


                            tableLayout.addView(tr_head, new TableLayout.LayoutParams(
                                    TableLayout.LayoutParams.FILL_PARENT,
                                    TableLayout.LayoutParams.WRAP_CONTENT));
                            for (int i = 0; i < orders.size(); i++) {
                                Log.e("TAG", orders.get(i).getC_Number());
                                TableRow tr = new TableRow(getApplicationContext());
                                tr.setLayoutParams(new TableRow.LayoutParams(
                                        TableRow.LayoutParams.FILL_PARENT,
                                        0));

                                TextView labelPRODUCTS = new TextView(getApplicationContext());
                                for (int j = 0; j < orders.get(i).getProducts().size(); j++) {
                                    labelPRODUCTS.append((j + 1) + "- " + orders.get(i).getProducts().get(j).getName() + "\n");
                                }
                                label_product.setTypeface(null, Typeface.BOLD);
                                label_product.setTextColor(R.color.colorBLack);
                                labelPRODUCTS.setGravity(Gravity.CENTER);
                                tr.addView(labelPRODUCTS);

                                TextView labelCNUMBER = new TextView(getApplicationContext());
                                labelCNUMBER.setText(orders.get(i).getC_Number());
                                labelCNUMBER.setTextColor(R.color.colorBLack);
                                labelCNUMBER.setTypeface(null, Typeface.BOLD);
                                labelCNUMBER.setGravity(Gravity.CENTER);
                                tr.addView(labelCNUMBER);

                                TextView labelDATE = new TextView(getApplicationContext());
                                String date = orders.get(i).getDate();
                                String[] parts = date.split("T");
                                String part1 = parts[0];
                                labelDATE.setText(part1);
                                labelDATE.setPadding(10, 10, 10, 10);
                                labelDATE.setTextColor(R.color.colorBLack);
                                labelDATE.setTypeface(null, Typeface.BOLD);
                                labelDATE.setGravity(Gravity.CENTER);
                                tr.addView(labelDATE);
                                if (i%2 == 0) {
                                    tr.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                                }else {
                                    tr.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                                }
                                tableLayout.addView(tr, new TableLayout.LayoutParams(
                                        TableLayout.LayoutParams.FILL_PARENT,
                                        0));

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserInfo> call, Throwable t) {
                    Log.e("TAG", t.getLocalizedMessage());
                }
            });
        }


    }
}
