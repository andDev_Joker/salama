package com.example.eljoker.salama.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eljoker.salama.R;
import com.example.eljoker.salama.models.products_model;
import com.example.eljoker.salama.product_types;
import com.example.eljoker.salama.show_products;

import java.util.ArrayList;

public class products_Adapter extends RecyclerView.Adapter<products_Adapter.ViewHolder>
{
    public products_Adapter(ArrayList<products_model> arrayList, Context context)
    {
        this.arrayList = arrayList;
        this.context = context;
    }

    ArrayList<products_model> arrayList;
    Context context;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.product_row, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        final products_model model = arrayList.get(position);
        holder.product_name.setText(model.getProduct_name());
        Glide.with(context).load(model.getProduct_img()).into(holder.product_img);
        holder.setItemClickListener(new ItemClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                String id = model.getProduct_id();
                Intent intent = new Intent(context, product_types.class);
                intent.putExtra("id", id);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        ItemClickListener itemClickListener;
        ImageView product_img;
        TextView product_name;
        LinearLayout parentLayout_show_products;
        public ViewHolder(View itemView)
        {
            super(itemView);
            product_img = itemView.findViewById(R.id.product_img);
            product_name = itemView.findViewById(R.id.product_name);
            parentLayout_show_products = itemView.findViewById(R.id.parentLayout_show_products);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }


        @Override
        public void onClick(View v)
        {
            itemClickListener.onClick(v, getAdapterPosition());
        }
    }

    private void openDetailActivity(String...details)
    {
        Intent i=new Intent(context, product_types.class);
        for (int x = 0; x < arrayList.size(); x++)
        {
            i.putExtra("NAME_KEY",details[x]);
        }
        context.startActivity(i);

    }
}
