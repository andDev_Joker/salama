package com.example.eljoker.salama;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eljoker.salama.adapters.About_Adapter;
import com.example.eljoker.salama.adapters.offers_Adapter;
import com.example.eljoker.salama.models.About_Model;
import com.example.eljoker.salama.models.offer_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AboutActivity extends AppCompatActivity {
    Typeface font;
    TextView tvAbout,tvDetails;
    String url;
    RecyclerView rv_about;
    ArrayList<About_Model> arrayList;
    RequestQueue mRequestQueue;
    About_Adapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        tvAbout=findViewById(R.id.tvAbout);
        tvDetails=findViewById(R.id.tvDetails);

        rv_about = findViewById(R.id.rv_about);
        arrayList = new ArrayList<>();
        adapter = new About_Adapter(getApplicationContext(), arrayList);
        rv_about.setAdapter(adapter);
        rv_about.setLayoutManager(new LinearLayoutManager(this));
        mRequestQueue = Volley.newRequestQueue(this);
        parsing();
    }

    private void parsing()
    {
        final ProgressDialog dialog =new ProgressDialog(this);
        dialog.setMessage("تحميل ....");
        dialog.show();
        url = "http://salamakamel-001-site1.etempurl.com/api/manager/Photos";
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response)
            {
                for (int i = 0; i < response.length(); i++)
                {
                    try {
                        JSONObject object = response.getJSONObject(i);
                        String photo = object.getString("Photo");
                        About_Model model = new About_Model();
                        model.setPhoto(photo);
                        arrayList.add(model);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
                dialog.dismiss();
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }
}


/*
    private void setFonts() {
        tvAbout.setTypeface(font);
        tvDetails.setTypeface(font);
    }

    private void initView() {
        font= Typeface.createFromAsset(getAssets(), "fonts/HS-Almidad-Thin.otf");

    }
*/
