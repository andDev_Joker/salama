package com.example.eljoker.salama.models;

import java.util.List;

public class UserInfo {
    private int ID;
    private String Name,Phone;
    private List<Orders> Orders;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public List<Orders> getOrders() {
        return Orders;
    }

    public void setOrders(List<Orders> orders) {
        Orders = orders;
    }
}
