package com.example.eljoker.salama.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eljoker.salama.R;
import com.example.eljoker.salama.models.offer_model;

import java.util.ArrayList;

public class offers_Adapter extends RecyclerView.Adapter<offers_Adapter.myHolder>
{
    ArrayList<offer_model> list;
    Context context;

    public offers_Adapter(ArrayList<offer_model> list, Context context)
    {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.offers_row, parent, false);
        myHolder holder = new myHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holder, int position)
    {
        offer_model model = list.get(position);
        Glide.with(context).load(model.getPhoto()).into(holder.img_offer);
        holder.txt_offer.setText(model.getName());
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    class myHolder extends RecyclerView.ViewHolder
    {
        ImageView img_offer;
        TextView txt_offer;
        public myHolder(View itemView)
        {
            super(itemView);
            img_offer = itemView.findViewById(R.id.img_offer);
            txt_offer = itemView.findViewById(R.id.txt_offer);
        }
    }
}
