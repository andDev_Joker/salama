package com.example.eljoker.salama;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.eljoker.salama.adapters.product_type_apdapter;
import com.example.eljoker.salama.models.product_type_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class product_types extends AppCompatActivity
{
    String id;
    TextView ProductType_Toolbar_Name;
    String url;
    Toolbar toolbar;
    RecyclerView rv_types;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    RequestQueue mRequestQueue;
    product_type_apdapter adapter;
    ArrayList<product_type_model> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_types);
        ProductType_Toolbar_Name = findViewById(R.id.ProductType_Toolbar_Name);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println("id ==" + id);
        url = "http://salamakamel-001-site1.etempurl.com/api/Manager/GetProducts/"+ id;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rv_types = findViewById(R.id.rv_types);
        arrayList = new ArrayList<>();
        adapter = new product_type_apdapter(this ,arrayList);
        rv_types.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_types.setItemAnimator(new DefaultItemAnimator());
        dividerItemDecoration = new DividerItemDecoration(rv_types.getContext(), linearLayoutManager.getOrientation());

        rv_types.setHasFixedSize(true);
        rv_types.setLayoutManager(linearLayoutManager);
        rv_types.addItemDecoration(dividerItemDecoration);
        mRequestQueue = Volley.newRequestQueue(this);
        jsonparse();
    }

    public void jsonparse()
    {
        final ProgressDialog dialog =new ProgressDialog(this);
        dialog.setMessage("تحميل ....");
        dialog.show();
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response)
            {
                System.out.print("aaa"+id);
                for (int i = 0; i < response.length(); i++)
                {
                    try {

                        JSONObject object = response.getJSONObject(i);

                        product_type_model model = new product_type_model();
                        model.setType_name(object.getString("Name"));
                        model.setType_img(object.getString("Photo"));
                        String id = object.getString("ID");
                        model.setType_id(id);
//                        model.setType_desc(object.getString("Description"));
                        model.setType_km(object.getInt("KMNumber"));
                        model.setType_price((object.getInt("Price")));
                        ProductType_Toolbar_Name.setText(object.getString("TypeName"));

                        arrayList.add(model);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss();
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

            }
        });
/*
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ID", String.valueOf(id));
                return params;
            }
        };
*/
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}
