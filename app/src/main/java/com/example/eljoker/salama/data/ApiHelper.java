package com.example.eljoker.salama.data;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {

    private static final String BASE_URL = "http://salamakamel-001-site1.etempurl.com/api/Manager/";


    private static Api apiService;

    public static Api getApiService() {
        if (apiService == null) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();
            apiService = retrofit.create(Api.class);
        }
        return apiService;
    }
}
