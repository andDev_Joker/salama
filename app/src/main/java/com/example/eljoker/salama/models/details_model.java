package com.example.eljoker.salama.models;

public class details_model
{
    private String name;
    private String img;
    private String desc;
    private int id;
    private int price;
    private int km;

    public details_model()
    {
    }

    public details_model(String name, String img, String desc, int id, int price, int km)
    {
        this.name = name;
        this.img = img;
        this.desc = desc;
        this.id = id;
        this.price = price;
        this.km = km;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }
}
