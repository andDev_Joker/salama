package com.example.eljoker.salama.models;

import java.util.List;

public class Orders {

    private int ID;
    private String Date,C_Number;
    private List<Products> Products;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getC_Number() {
        return C_Number;
    }

    public void setC_Number(String c_Number) {
        C_Number = c_Number;
    }

    public List<Products> getProducts() {
        return Products;
    }

    public void setProducts(List<Products> products) {
        Products = products;
    }
}
