package com.example.eljoker.salama;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.eljoker.salama.adapters.products_Adapter;
import com.example.eljoker.salama.models.products_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class show_products extends AppCompatActivity
{
//    Toolbar toolbar;
    RecyclerView rv;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    RequestQueue mRequestQueue;
    products_Adapter adapter;
    ArrayList<products_model> arrayList;
    String url = "http://salamakamel-001-site1.etempurl.com/api/Manager/GetAllTypes";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_products);
/*
        toolbar = findViewById(R.id.toolbar_products);
        setSupportActionBar(toolbar);
*/
        rv = findViewById(R.id.rv);
        arrayList = new ArrayList<>();
        adapter = new products_Adapter(arrayList ,getApplicationContext());
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(rv.getContext(), linearLayoutManager.getOrientation());

        rv.setHasFixedSize(true);
        rv.setLayoutManager(linearLayoutManager);
        rv.addItemDecoration(dividerItemDecoration);
        mRequestQueue = Volley.newRequestQueue(this);
        jsonParsing();
    }

    public void jsonParsing()
    {
        final ProgressDialog dialog =new ProgressDialog(this);
        dialog.setMessage("تحميل ....");
        dialog.show();

        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++)
                {
                    try {
                        JSONObject object = response.getJSONObject(i);
                        products_model model = new products_model();
                        String id = object.getString("ID");
                        model.setProduct_id(id);
                        model.setProduct_name(object.getString("Name"));
                        model.setProduct_img(object.getString("Photo"));
                        arrayList.add(model);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                dialog.dismiss();

                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
}
